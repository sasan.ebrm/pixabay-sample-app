package com.sebrahimi.ui.util

import androidx.compose.ui.unit.dp

object Spacing {
    val TINY = 4.dp
    val SMALL = 8.dp
    val NORMAL = 12.dp
    val LARGE = 18.dp
    val XLARGE = 24.dp
    val XX_LARGE = 30.dp
}