package com.sebrahimi.ui.util

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFDD99FF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)

val LightGray = Color(0xFFF8F8F8)
val Gray = Color(0xFFE0E0E0)
val LightBlue = Color(0xFFEEE3FF)