package com.sebrahimi.ui.util

import androidx.compose.ui.unit.dp

object Shape {
    val CARD_CORNER_RADIUS = 30.dp
    val CARD_CORNER_RADIUS_EXTENSIVE = 38.dp
    val CARD_CONTENT_PADDING = 22.dp
    val CARD_CONTENT_PADDING_EXTENSIVE = 38.dp
    val CARD_ELEVATION_EXTREME = 15.dp
    val CARD_ELEVATION_NORMAL = 10.dp
    val CARD_ELEVATION_LIGHT = 8.dp
    val CTA_BUTTON_CORNER_RADIUS = 14.dp
}