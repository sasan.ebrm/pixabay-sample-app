package com.sebrahimi.ui.kit

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.sebrahimi.ui.util.LightBlue
import com.sebrahimi.ui.util.Spacing

@Composable
fun UserChipView(name: String, profileImageUrl: String, imageSize: Dp = 40.dp) {
    Row(verticalAlignment = Alignment.CenterVertically) {
        CircularImageView(url = profileImageUrl, size = imageSize)
        Box(
            modifier = Modifier
                .padding(Spacing.NORMAL)
                .background(
                    LightBlue, shape = RoundedCornerShape(100)
                ),
        ) {
            Text(
                text = name,
                modifier = Modifier.padding(vertical = Spacing.TINY, horizontal = Spacing.NORMAL),
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
                style = MaterialTheme.typography.body2
            )
        }
    }
}