package com.sebrahimi.ui.kit

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.sebrahimi.ui.util.Spacing

@Composable
fun Chip(label: String) {
    Box(
        modifier = Modifier
            .background(MaterialTheme.colors.secondary, MaterialTheme.shapes.medium)
            .padding(horizontal = Spacing.SMALL, vertical = Spacing.TINY),
    ) {
        Text(
            text = label,
            color = MaterialTheme.colors.onPrimary,
            style = MaterialTheme.typography.caption
        )
    }
}
