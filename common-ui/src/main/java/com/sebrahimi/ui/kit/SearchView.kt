package com.sebrahimi.ui.kit

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import com.sebrahimi.ui.util.Shape
import com.sebrahimi.ui.util.Spacing

@Composable
fun SearchView(
    modifier: Modifier = Modifier,
    onSearchValueChanged: (txt: String) -> Unit,
    initialValue: String = "",
    placeHolder: String = "Search Keywords"
) {
    var text by rememberSaveable { mutableStateOf((initialValue)) }
    Card(
        modifier = Modifier
            .padding(Spacing.XLARGE)
            .fillMaxWidth(),
        shape = RoundedCornerShape(100),
        elevation = Shape.CARD_ELEVATION_LIGHT,
    ) {
        Row(verticalAlignment = Alignment.CenterVertically) {
            Icon(
                modifier = Modifier
                    .padding(start = Spacing.NORMAL)
                    .size(size = 26.dp),
                imageVector = Icons.Default.Search,
                contentDescription = "",
                tint = Color.LightGray
            )

            TextField(
                modifier = modifier
                    .weight(1F),
                value = text,
                onValueChange = { newText ->
                    text = newText
                    onSearchValueChanged(newText)
                },
                colors = TextFieldDefaults.textFieldColors(
                    focusedIndicatorColor = Color.Transparent,
                    backgroundColor = Color.Transparent,
                    unfocusedIndicatorColor = Color.Transparent
                ),
                placeholder = {
                    Text(text = placeHolder)
                },
                trailingIcon = {
                    Icon(
                        modifier = Modifier
                            .padding(end = Spacing.NORMAL)
                            .size(size = 26.dp)
                            .clickable {
                                text = ""
                                onSearchValueChanged("")
                            },
                        imageVector = Icons.Default.Close,
                        contentDescription = "",
                        tint = Color.LightGray,
                    )
                }
            )
        }
    }

}