package com.sebrahimi.ui.kit

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.layout.Box
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.Alignment.Companion.BottomCenter
import com.sebrahimi.ui.util.Spacing

@Composable
fun ProgressIndicator(visible: Boolean = false) {
    if(visible)
        Box(
            modifier = Modifier
                .fillMaxSize()
        ) {
            CircularProgressIndicator(
                modifier = Modifier
                    .width(42.dp)
                    .height(42.dp)
                    .padding(Spacing.SMALL)
                    .align(BottomCenter),
                strokeWidth = 4.dp,
                color = MaterialTheme.colors.secondary
            )
        }
}