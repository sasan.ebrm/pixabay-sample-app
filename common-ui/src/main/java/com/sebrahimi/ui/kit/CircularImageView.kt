package com.sebrahimi.ui.kit

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.Dp
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.sebrahimi.ui.util.Gray


@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun CircularImageView(
    url: String,
    size: Dp,
) {
    GlideImage(
        modifier = Modifier
            .width(size)
            .height(size)
            .clip(CircleShape)
            .background(Gray),
        model = url,
        contentScale = ContentScale.Crop,
        contentDescription = ""
    )
}