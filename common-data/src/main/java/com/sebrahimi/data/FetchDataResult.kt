package com.sebrahimi.data

import com.sebrahimi.data.util.Mappable
import com.sebrahimi.data.util.mapToTargetList

/**
 * A wrapper class to identify the state of fetch effort
 * Can be used with any type of data source (Rest Api, Database, Local storage)
 */
sealed class FetchDataResult<out T> {
    data class Success<T>(val data: T) : FetchDataResult<T>()
    data class Failure(val error: Exception) : FetchDataResult<Nothing>()
}

/**
 * Maps the content of [FetchDataResult.Success] object to a model
 */
fun <T : Mappable<K>, K> FetchDataResult<T>.mapToTarget(): FetchDataResult<K> {
    return when (this) {
        is FetchDataResult.Failure -> this
        is FetchDataResult.Success -> FetchDataResult.Success(this.data.mapToTarget())
    }
}

/**
 * Maps a list of [FetchDataResult.Success] objects to a list of models
 */
fun <T : List<Mappable<K>>, K> FetchDataResult<T>.mapToTargetList(): FetchDataResult<List<K>> {
    return when (this) {
        is FetchDataResult.Failure -> this
        is FetchDataResult.Success -> FetchDataResult.Success(this.data.mapToTargetList())
    }
}
