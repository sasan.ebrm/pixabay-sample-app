package com.sebrahimi.data

/**
 * Variable names to be used while providing dependencies using Hilt
 * To be used with @Named annotation in Hilt providers
 */
object DIKeys {
    const val BASE_URL = "BASE_URL"
    const val API_KEY = "API_KEY"
}