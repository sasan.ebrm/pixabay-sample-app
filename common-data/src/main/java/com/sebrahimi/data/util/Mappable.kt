package com.sebrahimi.data.util

/**
 * DTO, Entity, etc data objects that can be mapped to a model should implement this interface
 * This method converts an object (DTO, Entity, etc) to a desired model to be passed to UI/View layer
 */
interface Mappable<T> {
    fun mapToTarget(): T
}

fun <T> List<Mappable<T>>.mapToTargetList() : List<T> {
    return map {
        it.mapToTarget()
    }
}