package com.sebrahimi.pixabaysampleapp

sealed class Screen(val route: String) {
    object PhotoList: Screen("photo_list_screen")
    object PhotoDetails: Screen("photo_details_screen")

    fun withArgs(args: List<String>): String {
        return buildString {
            append(route)
            args.forEach {arg ->
                append("/$arg")
            }
        }
    }
}