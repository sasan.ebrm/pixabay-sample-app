package com.sebrahimi.pixabaysampleapp.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import com.sebrahimi.ui.util.Purple40
import com.sebrahimi.ui.util.Purple80
import com.sebrahimi.ui.util.PurpleGrey40
import com.sebrahimi.ui.util.PurpleGrey80

private val DarkColors = darkColors(
    primary = Purple80,
    secondary = PurpleGrey80
)

private val LightColors = lightColors(
    primary = Purple40,
    secondary = PurpleGrey40



    /* Other default colors to override
    background = Color(0xFFFFFBFE),
    surface = Color(0xFFFFFBFE),
    onPrimary = Color.White,
    onSecondary = Color.White,
    onTertiary = Color.White,
    onBackground = Color(0xFF1C1B1F),
    onSurface = Color(0xFF1C1B1F),
    */
)

@Composable
fun PixabaySampleAppTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    val colors = if (darkTheme) {
        DarkColors
    } else {
        LightColors
    }

    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}