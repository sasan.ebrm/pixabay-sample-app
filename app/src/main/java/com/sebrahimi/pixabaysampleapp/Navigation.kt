package com.sebrahimi.pixabaysampleapp

import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import androidx.paging.compose.LazyPagingItems
import androidx.paging.compose.collectAsLazyPagingItems
import com.sebrahimi.searchphoto.data.model.PhotoModel
import com.sebrahimi.searchphoto.view.photodetail.PhotoDetailScreen
import com.sebrahimi.searchphoto.view.photodetail.PhotoDetailViewModel
import com.sebrahimi.searchphoto.view.photolist.PhotoListViewModel
import com.sebrahimi.searchphoto.view.photolist.composables.PhotoListScreen

@Composable
fun Navigation(
) {
    val navController = rememberNavController()
    val homeScreen = Screen.PhotoList.route

    NavHost(navController = navController, startDestination = homeScreen) {

        /**
         * Photo List Screen
         */
        composable(route = Screen.PhotoList.route) {
            val viewModel = hiltViewModel<PhotoListViewModel>()
            val lazyPhotoItems: LazyPagingItems<PhotoModel> =
                viewModel.photos.collectAsLazyPagingItems()
            PhotoListScreen(
                lazyPhotoItems = lazyPhotoItems,
                initialSearchValue = "Fruits",
                onError = { /*TODO*/},
                onItemClicked = {
                    navController.navigate(
                        Screen.PhotoDetails.withArgs(listOf("${it.id}"))
                    )
                },
                onSearchValueChanged = {
                    viewModel.onSearchQueryChanged(it)
                })
        }

        /**
         * Photo Detail Screen
         */
        composable(Screen.PhotoDetails.route + "/{id}", arguments = listOf(navArgument("id") {
            type = NavType.LongType
            defaultValue = 0
            nullable = false
        })) { entry ->
            val viewModel = hiltViewModel<PhotoDetailViewModel>()
            PhotoDetailScreen(id = entry.arguments?.getLong("id"), viewModel) {
                navController.popBackStack()
            }
        }

    }
}

