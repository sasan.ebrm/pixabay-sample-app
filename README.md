# Pixabay Sample App

A sample Android application that fetches a list of items from Rest Api and shows them in an infinite list.

## Demo
<img src="https://gitlab.com/sasan.ebrm/pixabay-sample-app/-/raw/main/readme/demo.gif?raw=true" width="300">

## Architecture

The project has been divided to some smaller modules as shown bellow.

<img src="https://gitlab.com/sasan.ebrm/pixabay-sample-app/-/raw/main/readme/app_arch.jpg" width="700">

