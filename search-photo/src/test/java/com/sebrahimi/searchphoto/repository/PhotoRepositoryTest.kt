package com.sebrahimi.searchphoto.repository

import com.sebrahimi.data.FetchDataResult
import com.sebrahimi.data.mapToTarget
import com.sebrahimi.data.mapToTargetList
import com.sebrahimi.data.util.mapToTargetList
import com.sebrahimi.searchphoto.TestUtils
import com.sebrahimi.searchphoto.data.dto.PhotoListResponseDTO
import com.sebrahimi.searchphoto.datasource.PhotoLocalDataSource
import com.sebrahimi.searchphoto.datasource.PhotoRestApiDataSource
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Test
import org.mockito.Mockito.*

class PhotoRepositoryTest {
    private val restApiDataSource = mock(PhotoRestApiDataSource::class.java)
    private val localDataSource = mock(PhotoLocalDataSource::class.java)
    private val photoRepository = PhotoRepository(restApiDataSource, localDataSource)

    @Test
    fun `searchImages() returns data from rest api data source when shouldUseCache is false`() = runBlocking {
        val searchQuery = "test"
        val pageNumber = 1
        val pageSize = 20
        val restApiResult = FetchDataResult.Success(PhotoListResponseDTO(pageSize,200, TestUtils.generatePhotoDTOListByUserName(pageSize, "restapi")))
        `when`(restApiDataSource.searchPhotos(searchQuery, pageNumber, pageSize)).thenReturn(restApiResult)

        val localDatabaseResult = FetchDataResult.Success( TestUtils.generatePhotoEntityListByUserName(pageSize, "localdb"))
        `when`(localDataSource.searchPhotos(searchQuery, pageNumber, pageSize)).thenReturn(localDatabaseResult)

        val result = photoRepository.searchImages(searchQuery, pageNumber, pageSize, shouldUseCache = false)

        assertEquals(restApiResult.mapToTarget().mapToTargetList(), (result as FetchDataResult.Success))
        assertNotEquals(localDatabaseResult.mapToTargetList(), (result))
    }


    @Test
    fun `searchImages() returns data from local data source when shouldUseCache is true`() = runBlocking {
        val searchQuery = "test"
        val pageNumber = 1
        val pageSize = 20
        val restApiResult = FetchDataResult.Success(PhotoListResponseDTO(pageSize,200, TestUtils.generatePhotoDTOListByUserName(pageSize, "restapi")))
        `when`(restApiDataSource.searchPhotos(searchQuery, pageNumber, pageSize)).thenReturn(restApiResult)

        val localDatabaseResult = FetchDataResult.Success( TestUtils.generatePhotoEntityListByUserName(pageSize, "localdb"))
        `when`(localDataSource.searchPhotos(searchQuery, pageNumber, pageSize)).thenReturn(localDatabaseResult)

        val result = photoRepository.searchImages(searchQuery, pageNumber, pageSize, shouldUseCache = true)

        assertEquals(localDatabaseResult.mapToTargetList(), (result))
        assertNotEquals(restApiResult.mapToTarget().mapToTargetList(), (result as FetchDataResult.Success))
    }

    @Test
    fun `searchImages() returns appropriate list when number of data is zero,less or equal to data from datasource`() = runBlocking {
        val searchQuery = "test"
        val pageSize = 20

        val restApiResultPage1 = FetchDataResult.Success(PhotoListResponseDTO(20,32, TestUtils.generatePhotoDTOListByUserName(20)))
        val restApiResultPage2 = FetchDataResult.Success(PhotoListResponseDTO(12,32, TestUtils.generatePhotoDTOListByUserName(12)))
        val restApiResultPage3 = FetchDataResult.Success(PhotoListResponseDTO(0,32, emptyList()))

        `when`(restApiDataSource.searchPhotos(searchQuery, 1, pageSize)).thenReturn(restApiResultPage1)
        `when`(restApiDataSource.searchPhotos(searchQuery, 2, pageSize)).thenReturn(restApiResultPage2)
        `when`(restApiDataSource.searchPhotos(searchQuery, 3, pageSize)).thenReturn(restApiResultPage3)

        val resultPage1 = photoRepository.searchImages(searchQuery, 1, pageSize, shouldUseCache = false)
        val resultPage2 = photoRepository.searchImages(searchQuery, 2, pageSize, shouldUseCache = false)
        val resultPage3 = photoRepository.searchImages(searchQuery, 3, pageSize, shouldUseCache = false)

        assertEquals((resultPage1 as FetchDataResult.Success).data, restApiResultPage1.data.hits.mapToTargetList().mapToTargetList())
        assertEquals((resultPage2 as FetchDataResult.Success).data, restApiResultPage2.data.hits.mapToTargetList().mapToTargetList())
        assertEquals((resultPage3 as FetchDataResult.Success).data, restApiResultPage3.data.hits.mapToTargetList().mapToTargetList())
    }


    @Test
    fun `searchPhotoById() returns photo model when the id is available in the database`() = runBlocking {

        val localDatabaseResult = FetchDataResult.Success( TestUtils.generatePhotoEntityListByUserName(1, "localdb", 5).first())
        `when`(localDataSource.searchPhotoById(5)).thenReturn(localDatabaseResult)

        val result = photoRepository.searchPhotoById(5)

        assertEquals(localDatabaseResult::class.java, FetchDataResult.Success::class.java)
        result as FetchDataResult.Success
        assertEquals(localDatabaseResult.data.mapToTarget(), result.data)
    }

}