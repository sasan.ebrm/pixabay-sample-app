package com.sebrahimi.searchphoto

import com.sebrahimi.searchphoto.data.dto.PhotoDTO
import com.sebrahimi.searchphoto.data.entity.PhotoEntity
import com.sebrahimi.searchphoto.data.model.PhotoModel

object TestUtils {

    fun generatePhotoModelListByUserName(length: Int, user: String = "User", idOffset: Int = 0): List<PhotoModel> {
        val result = mutableListOf<PhotoModel>()
        repeat(length) {
            result.add(
                PhotoModel(
                    (it + idOffset).toLong(),
                    listOf("",""),
                    "",
                    "",
                    1,
                    1,
                    1,
                    "$user $it",
                    ""
                )
            )
        }
        return result
    }
    fun generatePhotoDTOListByUserName(length: Int, user: String = "User", idOffset: Int = 0): List<PhotoDTO> {
        val result = mutableListOf<PhotoDTO>()
        repeat(length) {
            result.add(
                PhotoDTO(
                    (it + idOffset).toLong(),
                    "",
                    "",
                    "",
                    1,
                    1,
                    1,
                    "$user $it",
                    ""
                )
            )
        }
        return result
    }
    fun generatePhotoEntityListByUserName(length: Int, user: String = "User", idOffset: Int = 0): List<PhotoEntity> {
        val result = mutableListOf<PhotoEntity>()
        repeat(length) {
            result.add(
                PhotoEntity(
                    (it + idOffset).toLong(),
                    "",
                    "",
                    "",
                    1,
                    1,
                    1,
                    "$user $it",
                    ""
                )
            )
        }
        return result
    }

}