package com.sebrahimi.searchphoto.repository

import com.sebrahimi.data.FetchDataResult
import com.sebrahimi.searchphoto.data.model.PhotoModel
import kotlinx.coroutines.flow.Flow

interface PhotoRepositoryInterface {
    /**
     * Fetches data from any type of data-source an returns a [FetchDataResult] containing final result
     * @param searchQuery keyword to be searched
     * @param pageNumber number of the page to be fetched
     * @param pageSize number of items per page
     * @param shouldUseCache specifies if the data should be fetched from cached data in local database or not
     */
    suspend fun searchImages(searchQuery: String, pageNumber: Int, pageSize: Int, shouldUseCache: Boolean = false) : FetchDataResult<List<PhotoModel>>
}