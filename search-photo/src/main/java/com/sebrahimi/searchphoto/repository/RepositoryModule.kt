package com.sebrahimi.searchphoto.repository

import com.sebrahimi.searchphoto.datasource.PhotoLocalDataSource
import com.sebrahimi.searchphoto.datasource.PhotoRestApiDataSource
import com.sebrahimi.searchphoto.network.PhotoRestApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent

@Module
@InstallIn(ActivityRetainedComponent::class)
object RepositoryModule {

    @Provides
    fun providePhotoRestApiDataSource(photoRestApi: PhotoRestApi): PhotoRestApiDataSource {
        return  PhotoRestApiDataSource(photoRestApi)
    }


    @Provides
    fun providePhotoRepository(photoRestApiDataSource: PhotoRestApiDataSource,photoLocalDataSource: PhotoLocalDataSource): PhotoRepository {
        return PhotoRepository(photoRestApiDataSource, photoLocalDataSource)
    }

}