package com.sebrahimi.searchphoto.repository

import com.sebrahimi.data.FetchDataResult
import com.sebrahimi.data.mapToTarget
import com.sebrahimi.data.mapToTargetList
import com.sebrahimi.data.util.mapToTargetList
import com.sebrahimi.searchphoto.data.dto.PhotoListResponseDTO
import com.sebrahimi.searchphoto.data.model.PhotoModel
import com.sebrahimi.searchphoto.datasource.PhotoLocalDataSource
import com.sebrahimi.searchphoto.datasource.PhotoRestApiDataSource
import javax.inject.Inject

/**
 * This class will interact  with all types of data-sources to perform CRUD and provide data in a unique format to other layers
 */
class PhotoRepository @Inject constructor(
    private val photoRestApiDataSource: PhotoRestApiDataSource,
    private val photoLocalDataSource: PhotoLocalDataSource,
) : PhotoRepositoryInterface {
    override suspend fun searchImages(
        searchQuery: String,
        pageNumber: Int,
        pageSize: Int,
        shouldUseCache: Boolean
    ): FetchDataResult<List<PhotoModel>> {
        if (!shouldUseCache) {
            val restApiResult =
                photoRestApiDataSource.searchPhotos(searchQuery, pageNumber, pageSize)
            saveApiResultToDatabase(restApiResult, searchQuery, pageNumber)
            return restApiResult.mapToTarget().mapToTargetList()
        }
        return photoLocalDataSource.searchPhotos(searchQuery, pageNumber, pageSize)
            .mapToTargetList()
    }

    suspend fun searchPhotoById(id: Long): FetchDataResult<PhotoModel> {
        return photoLocalDataSource.searchPhotoById(id).mapToTarget()
    }

    /**
     * Saves the data retrived from Rest Api to local database to be cached
     * and used when offline
     * @param restApiResult the response fetched from Rest Api
     * @param searchQuery the keyword to be searched
     * @param pageNumber the number of page to be fetched
     */
    private suspend fun saveApiResultToDatabase(
        restApiResult: FetchDataResult<PhotoListResponseDTO>,
        searchQuery: String,
        pageNumber: Int
    ) {
        if (restApiResult is FetchDataResult.Success) {
            photoLocalDataSource.upsertPhotos(
                restApiResult.data.hits.mapToTargetList(),
                searchQuery
            )
        }
    }
}
