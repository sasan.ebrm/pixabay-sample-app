package com.sebrahimi.searchphoto.view.photolist

import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.sebrahimi.data.FetchDataResult
import com.sebrahimi.searchphoto.data.model.PhotoModel
import com.sebrahimi.searchphoto.repository.PhotoRepository
import javax.inject.Inject

/**
 * [PagingSource] to fetch data from [PhotoRepository]
 * This class mainly handles the pagination of data and
 * to fetch data directly calls [PhotoRepository.searchImages] method
 * If an error happens while fetching data, it shouts an [Exception]
 * @param photoRepository the repository to request data from
 * @param searchQuery the query that the data should be fetched based on that
 * @param pageSize the number of fetched items per page
 */
class PhotoListPagingSource @Inject constructor(
    private val photoRepository: PhotoRepository,
    private val searchQuery: String,
    private val pageSize: Int,
    private val isInternetAvailable: () -> Boolean
) : PagingSource<Int, PhotoModel>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, PhotoModel> {
        try {
            val page = params.key ?: 1
            val shouldUseCacheData = !isInternetAvailable()
            return when (val photos = photoRepository.searchImages(searchQuery, page, pageSize, shouldUseCacheData)) {
                is FetchDataResult.Failure -> {
                    LoadResult.Error(photos.error)
                }
                is FetchDataResult.Success -> {
                    LoadResult.Page(
                        data = photos.data,
                        prevKey = if (page == 1) null else page.minus(1),
                        nextKey = if (photos.data.isEmpty()) null else page.plus(1)
                    )
                }
            }
        } catch (e: Exception) {
            return LoadResult.Error(e)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, PhotoModel>): Int? {
        return state.anchorPosition?.let { position ->
            val page = state.closestPageToPosition(position)
            page?.prevKey?.minus(1) ?: page?.nextKey?.plus(1)
        }
    }
}
