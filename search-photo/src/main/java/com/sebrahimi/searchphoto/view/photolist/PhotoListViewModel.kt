package com.sebrahimi.searchphoto.view.photolist

import android.app.Application
import android.util.Log
import androidx.lifecycle.*
import androidx.paging.*
import com.sebrahimi.searchphoto.data.model.PhotoModel
import com.sebrahimi.searchphoto.repository.PhotoRepository
import com.sebrahimi.searchphoto.view.NetworkDependentViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@HiltViewModel
class PhotoListViewModel @Inject constructor(
    private val photoRepository: PhotoRepository,
    private val application: Application
) : NetworkDependentViewModel(application) {

    private val _pageSize = 20
    private val _searchQueryDebounce = 400L
    private val _queryString = MutableStateFlow("")

    /**
     * The list of image items to be shown in view layer.
     * It is wrapped firstly by [PagingData] to load data in small chunks and
     * secondly by [Flow] to stream the data to view
     * whenever [_queryString] changes,
     * the new value will be mapped to refresh this variable and
     * create new pagingSource based on new query string
     */
    val photos: Flow<PagingData<PhotoModel>> = _queryString
        .debounce(_searchQueryDebounce)
        .flatMapLatest { queryString ->
            Pager(PagingConfig(pageSize = _pageSize)) {
                PhotoListPagingSource(photoRepository, queryString, _pageSize, ::isOnline)
            }
                .flow
        }
        .cachedIn(viewModelScope)


    /**
     * This method is called when the value of searchQuery gets changed in view.
     * If the value is the same as existing value, it does not do anything.
     * Otherwise it updates the value of [_queryString]
     */
    fun onSearchQueryChanged(txt: String) {
        if (_queryString.value == txt) return
        _queryString.value = txt
    }

}


