package com.sebrahimi.searchphoto.view.photolist.composables

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.paging.LoadState
import androidx.paging.compose.LazyPagingItems
import com.sebrahimi.searchphoto.data.model.PhotoModel
import androidx.paging.compose.items
import com.sebrahimi.ui.kit.ProgressIndicator
import com.sebrahimi.ui.kit.SearchView

/**
 * Renders the screen to show list of fetched image items
 */
@Composable
fun PhotoListScreen(
    lazyPhotoItems: LazyPagingItems<PhotoModel>,
    onError: (e: Throwable) -> Unit,
    onItemClicked: (photoModel: PhotoModel) -> Unit,
    onSearchValueChanged: (txt: String) -> Unit,
    initialSearchValue: String = ""
) {

    /**
     * Renders the list of photo items
     * If the list is empty, renders a simple text message
     */
    Column {
        SearchView(onSearchValueChanged = onSearchValueChanged, initialValue = initialSearchValue)
        if (lazyPhotoItems.itemCount > 0)
            PhotoList(lazyPhotoItems = lazyPhotoItems, onItemClicked = onItemClicked)
        else
            EmptyListMessage()
    }

    /**
     * If the load state is [LoadState.Loading] (either initially or for fetching more pages)
     * shows a loading indicator at the bottom of the screen
     */
    ProgressIndicator(
        lazyPhotoItems.loadState.append is LoadState.Loading ||
                lazyPhotoItems.loadState.refresh is LoadState.Loading
    )

    /**
     * Initially triggers a search to load initial data
     */
    LaunchedEffect(Unit) {
        onSearchValueChanged(initialSearchValue)
    }
}

/**
 * Renders the list of photo items
 */
@Composable
private fun PhotoList(lazyPhotoItems: LazyPagingItems<PhotoModel>, onItemClicked: (photoModel: PhotoModel) -> Unit){
    LazyColumn {
        items(lazyPhotoItems) { item ->
            item?.let {
                PhotoListItem(photoModel = it, onItemClicked)
            }
        }
    }
}

/**
 * Renders empty list message
 */
@Composable
private fun EmptyListMessage(){
    Box(modifier = Modifier
        .fillMaxSize(),
    ) {
        Text(text = "No result !!!",
            style = TextStyle(color = Color.Gray)
            ,modifier = Modifier.align(Alignment.Center))
    }
}
