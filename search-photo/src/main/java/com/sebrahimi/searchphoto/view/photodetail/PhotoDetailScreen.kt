package com.sebrahimi.searchphoto.view.photodetail

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.runtime.*
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.sebrahimi.data.FetchDataResult
import com.sebrahimi.searchphoto.data.model.PhotoModel
import com.sebrahimi.ui.util.Size
import com.sebrahimi.ui.util.Spacing
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.zIndex
import com.sebrahimi.searchphoto.view.photolist.composables.TagList
import com.sebrahimi.ui.kit.UserChipView
import com.sebrahimi.ui.util.LightGray

/**
 * Renders whole screen to show photo item details
 */
@Composable
fun PhotoDetailScreen(id: Long?, viewModel: PhotoDetailViewModel, onBackButtonPressed: () -> Unit) {
    id?.let { photoId ->
        val item by viewModel.photoDetail.collectAsState(null)
        item?.let {
            when (it) {
                is FetchDataResult.Failure -> {
                    ErrorComposable(it.error, onBackButtonPressed)
                }
                is FetchDataResult.Success -> {
                    PhotoDetailView(photoModel = it.data, onBackButtonPressed)
                }
            }
        } ?: run {
            // TODO handle null item
        }
        LaunchedEffect(Unit) {
            viewModel.fetchData(photoId)
        }
    } ?: run {
        ErrorComposable(e = Exception("ID IS NULL !!!"), onBackButtonPressed)
    }
}

/**
 * Renders content of the screen to show photo item details
 */
@Composable
fun PhotoDetailView(photoModel: PhotoModel, onCloseClicked: () -> Unit) {
    Column(
        modifier = Modifier
            .padding(0.dp)
            .background(LightGray),
        verticalArrangement = Arrangement.SpaceBetween,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        MainImage(
            photoModel.webformatURL,
            modifier = Modifier
                .aspectRatio(1f)
                .fillMaxWidth()
                .zIndex(2f)
        )
        Spacer(modifier = Modifier.height(Spacing.XX_LARGE))
        Card(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(),
            shape = RoundedCornerShape(0.dp, Size.CARD_CORNER_RADIUS, 0.dp, 0.dp),
            elevation = Size.CARD_ELEVATION_EXTREME
        ) {
            Column(
                verticalArrangement = Arrangement.SpaceBetween,
                modifier = Modifier
                    .padding(Spacing.LARGE)
                    .verticalScroll(rememberScrollState())
            ) {
                Column() {
                    InfoView(photoModel = photoModel)
                }
                GoBackButton(onCloseClicked)
            }
        }
    }
}

/**
 * Renders the box to show containing photo item info
 */
@Composable
fun InfoView(photoModel: PhotoModel) {
    val rowModifier = Modifier
        .padding(vertical = Spacing.XLARGE)
        .fillMaxWidth()
    Box(
        modifier = Modifier
            .background(LightGray, shape = RoundedCornerShape(Size.INFO_CONTAINER_CORNER_RADIUS))
    ) {
        Column(
            modifier = Modifier
                .padding(horizontal = Spacing.XX_LARGE, vertical = 0.dp)
        ) {
            Row(modifier = rowModifier, verticalAlignment = Alignment.CenterVertically) {
                UserChipView(name = photoModel.user, profileImageUrl = photoModel.userImageURL)
            }
            Divider()
            Row(modifier = rowModifier, horizontalArrangement = Arrangement.SpaceBetween) {
                IconLabel(
                    resourceId = com.sebrahimi.ui.R.drawable.ic_favorite_48,
                    label = photoModel.likes.toString(),
                    Color.Red
                )
                IconLabel(
                    resourceId = com.sebrahimi.ui.R.drawable.ic_download_48,
                    label = photoModel.downloads.toString(),
                    Color.Blue
                )
                IconLabel(
                    resourceId = com.sebrahimi.ui.R.drawable.ic_eye_48,
                    label = photoModel.comments.toString()
                )

            }
            Divider()
            Row(modifier = rowModifier) {
                TagList(tags = photoModel.tags.map { "#${it.trim()}" })
            }
        }
    }
}

/**
 * Generates a single view containing an icon and a text in a row to show statistics like number of likes, etc
 */
@Composable
private fun IconLabel(resourceId: Int, label: String, color: Color = Color.DarkGray) {
    Row(verticalAlignment = Alignment.CenterVertically) {
        Icon(
            modifier = Modifier
                .height(22.dp)
                .width(22.dp),
            painter = painterResource(id = resourceId),
            contentDescription = "",
            tint = color
        )
        Text(modifier = Modifier.padding(start = Spacing.SMALL), text = label)
    }
}

@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun MainImage(
    imageUrl: String,
    modifier: Modifier
) {
    Card(
        modifier = modifier,
        shape = RoundedCornerShape(0.dp, 0.dp, Size.CARD_CORNER_RADIUS, 0.dp),
        elevation = Size.CARD_ELEVATION_LIGHT
    ) {
        GlideImage(
            model = imageUrl, contentDescription = "", contentScale = ContentScale.Crop,
            modifier = modifier.fillMaxSize()
        )
    }
}

/**
 * In case of an error, this composable should be rendered.
 * It renders a text presenting the error message and a button to go back
 */
@Composable
private fun ErrorComposable(e: Exception, onBackButtonPressed: () -> Unit) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight(),
        shape = RoundedCornerShape(0.dp, Size.CARD_CORNER_RADIUS, 0.dp, 0.dp),
        elevation = Size.CARD_ELEVATION_EXTREME
    ) {
        Column(
            verticalArrangement = Arrangement.Center,
            modifier = Modifier
                .padding(Spacing.LARGE)
        ) {
            Text(
                text = e.message ?: "SOMETHING WENT WRONG !!!",
                color = MaterialTheme.colors.error,
                textAlign = TextAlign.Center
            )
            Spacer(modifier = Modifier.height(Spacing.XLARGE))
            GoBackButton(onBackButtonPressed)
        }
    }
}

@Composable
fun GoBackButton(onClick: () -> Unit) {
    Button(
        modifier = Modifier
            .fillMaxWidth(),
        shape = RoundedCornerShape(Size.CTA_BUTTON_CORNER_RADIUS),
        onClick = {
            onClick()
        }
    ) {
        Text(text = "Go Back", modifier = Modifier.padding(Spacing.LARGE))
    }
}
