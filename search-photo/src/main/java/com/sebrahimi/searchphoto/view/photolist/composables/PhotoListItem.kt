package com.sebrahimi.searchphoto.view.photolist.composables

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import androidx.compose.ui.zIndex
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.sebrahimi.searchphoto.data.model.PhotoModel
import com.sebrahimi.ui.kit.Chip
import com.sebrahimi.ui.kit.UserChipView
import com.sebrahimi.ui.util.Shape
import com.sebrahimi.ui.util.Spacing


@Composable
fun PhotoListItem(photoModel: PhotoModel, onItemClicked: (photoModel: PhotoModel) -> Unit) {

    val elementsAroundSpacing = Spacing.LARGE
    val thumbHeight = 120.dp
    val thumbWidth = 120.dp

    Card(
        modifier = Modifier
            .padding(
                elementsAroundSpacing
            )
            .fillMaxWidth(),
        shape = RoundedCornerShape(Shape.CARD_CORNER_RADIUS),
        elevation = Shape.CARD_ELEVATION_LIGHT,

        ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceEvenly,
            modifier = Modifier
                .clickable(
                    enabled = true,
                    onClickLabel = "Clickable image",
                    onClick = {
                        onItemClicked(photoModel)
                    }
                )
                .fillMaxHeight(),

            ) {
            ThumbNailImage(
                photoModel.previewURL,
                modifier = Modifier
                    .height(thumbHeight)
                    .width(thumbWidth)
                    .zIndex(2f),
            )
            Column(
                modifier = Modifier
                    .fillMaxHeight()
                    .height(thumbHeight)
                    .padding(horizontal = Spacing.LARGE, vertical = Spacing.SMALL),
                verticalArrangement = Arrangement.SpaceAround
            ) {
                UserChipView(name = photoModel.user, profileImageUrl = photoModel.userImageURL)
                TagList(tags = photoModel.tags.map { "#${it.trim()}" })
            }
        }
    }
}

/**
 * Renders image thumbnail
 */
@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun ThumbNailImage(
    imageUrl: String,
    modifier: Modifier
) {
    Card(
        modifier = modifier,
        shape = RoundedCornerShape(Shape.CARD_CORNER_RADIUS, Shape.CARD_CORNER_RADIUS, 0.dp, 0.dp),
        elevation = Shape.CARD_ELEVATION_LIGHT
    ) {
        GlideImage(
            model = imageUrl, contentDescription = "", contentScale = ContentScale.Crop,
            modifier = Modifier.fillMaxSize()
        )
    }
}
/**
 * Renders the list of tags
 */
@Composable
fun TagList(tags: List<String>) {
    LazyRow(
        modifier = Modifier.fillMaxWidth(),
        contentPadding = PaddingValues(horizontal = 0.dp, vertical = Spacing.SMALL)
    ) {
        items(tags.size) { index ->
            Chip(label = tags[index])
            Spacer(modifier = Modifier.width(Spacing.SMALL))
        }
    }
}

