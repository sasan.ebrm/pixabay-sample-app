package com.sebrahimi.searchphoto.view.photodetail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sebrahimi.data.FetchDataResult
import com.sebrahimi.searchphoto.data.model.PhotoModel
import com.sebrahimi.searchphoto.repository.PhotoRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PhotoDetailViewModel @Inject constructor(
    private val photoRepository: PhotoRepository
) : ViewModel() {

    private val _photoDetail = MutableSharedFlow<FetchDataResult<PhotoModel>>()
    val photoDetail: SharedFlow<FetchDataResult<PhotoModel>> = _photoDetail

    /**
     * Fetches the item from repository for a specific id
     * @param id the id of item to be fetched
     */
    fun fetchData(id: Long){
        viewModelScope.launch {
            val photo = photoRepository.searchPhotoById(id)
            _photoDetail.emit(photo)
        }
    }
}