package com.sebrahimi.searchphoto.view

import android.content.Context
import androidx.lifecycle.ViewModel
import com.sebrahimi.network.util.NetworkUtils

import javax.inject.Inject

/**
 * Base viewmodel class for viewmodels that need to know the status of network access
 */
open class NetworkDependentViewModel @Inject constructor(private val context: Context) : ViewModel() {
    protected fun isOnline(): Boolean {
        return NetworkUtils.isDeviceOnline(context)
    }
}