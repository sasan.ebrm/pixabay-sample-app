package com.sebrahimi.searchphoto.db

import android.app.Application
import androidx.room.Room
import com.sebrahimi.searchphoto.datasource.PhotoLocalDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

/**
 * This module provides local database objects
 */
@Module
@InstallIn(SingletonComponent::class)
object DataBaseModule {

    @Provides
    fun providePhotoDatabase(application: Application): SearchPhotoDatabase {
        return Room.databaseBuilder(
            application,
            SearchPhotoDatabase::class.java, Constants.PHOTO_DATABASE_NAME
        ).build()
    }

    @Provides
    fun providePhotoDao(searchPhotoDatabase: SearchPhotoDatabase): PhotoDao {
        return searchPhotoDatabase.photoDao()
    }

    @Provides
    fun providePhotoLocalDataSource(photoDao: PhotoDao): PhotoLocalDataSource {
        return PhotoLocalDataSource(photoDao)
    }

}