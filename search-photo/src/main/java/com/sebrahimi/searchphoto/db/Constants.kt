package com.sebrahimi.searchphoto.db

object Constants {
    const val PHOTO_DATABASE_NAME = "PhotoDB"
    const val TABLE_PHOTO = "Photo"
    const val TABLE_PHOTO_SEARCH = "PhotoSearch"
}