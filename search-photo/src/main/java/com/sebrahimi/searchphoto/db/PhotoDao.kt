package com.sebrahimi.searchphoto.db

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Upsert
import com.sebrahimi.searchphoto.data.entity.PhotoEntity
import com.sebrahimi.searchphoto.data.entity.PhotoKeySearchEntity

@Dao
interface PhotoDao {

    @Upsert
    fun upsertListOfPhotos(photos: List<PhotoEntity>)

    @Upsert
    fun upsertPhotoKeySearches(photoKeySearchEntities: List<PhotoKeySearchEntity>)

    /**
     * Selects the items based on required keyword
     */
    @Query(
        """
        SELECT ${Constants.TABLE_PHOTO}.* FROM ${Constants.TABLE_PHOTO} 
        INNER JOIN ${Constants.TABLE_PHOTO_SEARCH} ON ${Constants.TABLE_PHOTO}.id = ${Constants.TABLE_PHOTO_SEARCH}.id_image 
        WHERE ${Constants.TABLE_PHOTO_SEARCH}.keyword LIKE '%' || :keyword || '%'
        LIMIT :limit OFFSET :offset
        """
    )
    suspend fun getPhotosByKeyword(keyword: String, limit: Int, offset: Int): List<PhotoEntity>

    /**
     * Selects a single item by id
     */
    @Query(
        """
            SELECT * FROM ${Constants.TABLE_PHOTO} WHERE id = :id
        """
    )
    suspend fun getPhotoById(id: Long): PhotoEntity

}