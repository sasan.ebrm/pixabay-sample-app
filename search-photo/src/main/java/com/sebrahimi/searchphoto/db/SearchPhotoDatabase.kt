package com.sebrahimi.searchphoto.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.sebrahimi.searchphoto.data.entity.PhotoEntity
import com.sebrahimi.searchphoto.data.entity.PhotoKeySearchEntity

@Database(entities = [PhotoEntity::class,PhotoKeySearchEntity::class], version = 1)
abstract class SearchPhotoDatabase: RoomDatabase() {
    abstract fun photoDao(): PhotoDao
}