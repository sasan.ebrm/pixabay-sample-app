package com.sebrahimi.searchphoto.data.model

data class PhotoModel(
    val id: Long,
    val tags: List<String>,
    val previewURL: String,
    val webformatURL: String,
    val downloads: Int,
    val likes: Int,
    val comments: Int,
    val user: String,
    val userImageURL: String
)

