package com.sebrahimi.searchphoto.data.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.sebrahimi.data.util.Mappable
import com.sebrahimi.searchphoto.data.model.PhotoModel
import com.sebrahimi.searchphoto.db.Constants

@Entity(tableName = Constants.TABLE_PHOTO)
data class PhotoEntity(
    @PrimaryKey val id: Long,
    val tags: String,
    val previewURL: String,
    val webformatURL: String,
    val downloads: Int,
    val likes: Int,
    val comments: Int,
    val user: String,
    val userImageURL: String
): Mappable<PhotoModel> {
    override fun mapToTarget(): PhotoModel {
        return PhotoModel(
            id,
            tags.split(","),
            previewURL,
            webformatURL,
            downloads,
            likes,
            comments,
            user,
            userImageURL
        )
    }
}