package com.sebrahimi.searchphoto.data.entity

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import com.sebrahimi.searchphoto.db.Constants

@Entity(
    tableName = Constants.TABLE_PHOTO_SEARCH,
    foreignKeys = [
        ForeignKey(
            entity = PhotoEntity::class,
            parentColumns = ["id"],
            childColumns = ["id_image"],
            onDelete = ForeignKey.CASCADE
        )
    ],
    indices = [Index(value = ["id_image","keyword"])]
)
data class PhotoKeySearchEntity(
    @PrimaryKey val id_image: Long,
    val keyword: String
)