package com.sebrahimi.searchphoto.data.dto

import com.sebrahimi.data.util.Mappable
import com.sebrahimi.searchphoto.data.entity.PhotoEntity
import com.sebrahimi.searchphoto.data.model.PhotoModel

data class PhotoDTO(
    val id: Long,
    val tags: String,
    val previewURL: String?,
    val webformatURL: String?,
    val downloads: Int?,
    val likes: Int?,
    val comments: Int?,
    val user: String?,
    val userImageURL: String?
): Mappable<PhotoEntity> {
    override fun mapToTarget() = PhotoEntity(
        id,
        tags,
        previewURL ?: "",
        webformatURL ?: "",
        downloads ?: 0,
        likes ?: 0,
        comments ?: 0,
        user ?: "Unknown",
        userImageURL ?: ""
    )
}
