package com.sebrahimi.searchphoto.data.dto

import com.sebrahimi.data.util.Mappable
import com.sebrahimi.data.util.mapToTargetList
import com.sebrahimi.searchphoto.data.entity.PhotoEntity

data class PhotoListResponseDTO(
    val total: Int,
    val totalHits: Int,
    val hits: List<PhotoDTO>
): Mappable<List<PhotoEntity>> {
    override fun mapToTarget(): List<PhotoEntity> {
        return this.hits.mapToTargetList()
    }
}
