package com.sebrahimi.searchphoto.datasource

import com.sebrahimi.data.FetchDataResult
import com.sebrahimi.network.process
import com.sebrahimi.searchphoto.data.dto.PhotoListResponseDTO
import com.sebrahimi.searchphoto.network.PhotoRestApi
import javax.inject.Inject

/**
 * This class interacts with [PhotoRestApi] to retrieve and/or send data to the cloud Api
 */
class PhotoRestApiDataSource @Inject constructor(
    private val photoRestApi: PhotoRestApi
) : PhotoDataSourceInterface<PhotoListResponseDTO> {

    override suspend fun searchPhotos(searchQuery: String, pageNumber: Int, pageSize: Int): FetchDataResult<PhotoListResponseDTO> =
        photoRestApi.searchPhoto(searchQuery, pageNumber, pageSize).process()

}