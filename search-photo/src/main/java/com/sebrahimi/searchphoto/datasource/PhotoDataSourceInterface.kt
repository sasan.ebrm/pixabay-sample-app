package com.sebrahimi.searchphoto.datasource

import com.sebrahimi.data.FetchDataResult

/**
 * Data sources should implement this interface to have similar api
 */
interface PhotoDataSourceInterface<out T> {

    /**
     * Requests the desired data-source (Rest API, Database, etc) based on passed query text
     * @param searchQuery query text to search photos based on that
     * @return [FetchDataResult] containing a Success or Failure object
     */
    suspend fun searchPhotos(searchQuery: String, pageNumber: Int, pageSize: Int): FetchDataResult<T>
}