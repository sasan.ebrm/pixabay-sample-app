package com.sebrahimi.searchphoto.datasource

import com.sebrahimi.data.FetchDataResult
import com.sebrahimi.searchphoto.db.PhotoDao
import com.sebrahimi.searchphoto.data.entity.PhotoEntity
import com.sebrahimi.searchphoto.data.entity.PhotoKeySearchEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

/**
 * This class is responsible to interact with database to do CRUD actions on tables related to photo
 */
class PhotoLocalDataSource @Inject constructor(
    private val photoDao: PhotoDao
) : PhotoDataSourceInterface<List<PhotoEntity>> {

    /**
     * Fetches a list of [PhotoEntity] from database
     * @param searchQuery the keyword to be searched in database
     * @param pageNumber is used as offset while selecting data from database
     * @param pageSize is used as limit while selecting data from database
     */
    override suspend fun searchPhotos(
        searchQuery: String,
        pageNumber: Int,
        pageSize: Int
    ): FetchDataResult<List<PhotoEntity>> {
        return try {
            FetchDataResult.Success(photoDao.getPhotosByKeyword(searchQuery, pageSize,pageNumber - 1))
        } catch (e: Exception) {
            FetchDataResult.Failure(e)
        }
    }

    suspend fun searchPhotoById(id: Long): FetchDataResult<PhotoEntity> {
        return try {
            FetchDataResult.Success(photoDao.getPhotoById(id))
        } catch(e: Exception){
            FetchDataResult.Failure(e)
        }
    }

    /**
     * Inserts (updates in case of previously existing row) the list of photo entity to database.
     */
    suspend fun upsertPhotos(photoEntities: List<PhotoEntity>, searchQuery: String){
        withContext(Dispatchers.IO){
            photoDao.upsertListOfPhotos(photoEntities)
            photoDao.upsertPhotoKeySearches(
                photoEntities.map {entity ->
                    PhotoKeySearchEntity(entity.id,searchQuery)
                }
            )
        }
    }
}