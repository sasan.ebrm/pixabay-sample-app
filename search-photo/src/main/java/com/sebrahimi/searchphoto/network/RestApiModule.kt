package com.sebrahimi.searchphoto.network

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit

@Module
@InstallIn(SingletonComponent::class)
object RestApiModule {

    @Provides
    fun providePhotoRestApi(
        retrofit: Retrofit
    ): PhotoRestApi {
        return retrofit.create(PhotoRestApi::class.java)
    }

}