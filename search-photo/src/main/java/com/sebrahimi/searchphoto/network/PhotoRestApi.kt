package com.sebrahimi.searchphoto.network

import com.sebrahimi.searchphoto.data.dto.PhotoListResponseDTO
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface PhotoRestApi {
    @GET("/api")
    suspend fun searchPhoto(@Query("q") searchQuery: String,@Query("page") pageNumber: Int,@Query("per_page") pageSize: Int = 20): Response<PhotoListResponseDTO>
}