package com.sebrahimi.network

import com.sebrahimi.data.FetchDataResult
import retrofit2.Response

object RetrofitHelper {

    /**
     * Checks the retorfit [Response] object to process the content of it and map to proper [FetchDataResult] object
     */
    fun <K> processRetrofitResponse(retrofitResponse: Response<K>): FetchDataResult<K> {
        return if (retrofitResponse.isSuccessful)
            if (retrofitResponse.body() != null) FetchDataResult.Success(retrofitResponse.body()!!)
            else handleHttpError(retrofitResponse.code(), "Null Response Body")
        else handleHttpError(retrofitResponse.code(), retrofitResponse.message())
    }

    /**
     * Generates an [FetchDataResult.Failure] object with a proper exception message
     */
    // TODO change error messages in a better format
    private fun handleHttpError(code: Int, message: String?): FetchDataResult.Failure {
        var errorMessage = if (code >= 500) "Server Side\nError Code: $code"
        else if (code >= 400) "Error Code: $code"
        else "Unknown Error\nError Code: $code"
        message?.let {
            errorMessage += "\nDetail: $message"
        }
        return FetchDataResult.Failure(Exception(errorMessage))
    }
}
