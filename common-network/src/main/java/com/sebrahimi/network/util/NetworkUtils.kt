package com.sebrahimi.network.util

import android.content.Context
import android.content.Context.CONNECTIVITY_SERVICE
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.NetworkInfo
import android.os.Build

object NetworkUtils {
    fun isDeviceOnline(context: Context): Boolean {
        val connManager = context.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val networkCapabilities = connManager.getNetworkCapabilities(connManager.activeNetwork)
            return if (networkCapabilities == null) {
                false
            } else {
                networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET) &&
                        networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_VALIDATED) &&
                        networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_NOT_SUSPENDED)
            }
        } else {
            // below Marshmallow
            val activeNetwork = connManager.activeNetworkInfo
            return if (activeNetwork?.isConnectedOrConnecting == true && activeNetwork.isAvailable) {
                when (activeNetwork.state) {
                    NetworkInfo.State.CONNECTED -> {
                        true
                    }
                    NetworkInfo.State.CONNECTING -> {
                        true
                    }
                    else -> {
                        false
                    }
                }
            } else {
                false
            }
        }
    }
}