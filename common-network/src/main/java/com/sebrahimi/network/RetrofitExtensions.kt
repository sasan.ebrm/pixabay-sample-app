package com.sebrahimi.network

import com.sebrahimi.data.FetchDataResult
import retrofit2.Response

/**
 * Extension function to make it possible to call process directly on a [Response] object
 */
fun <T> Response<T>.process(): FetchDataResult<T> {
    return RetrofitHelper.processRetrofitResponse(this)
}
